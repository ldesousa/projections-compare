# Creates spatial bins with dggridR.
#
# Author: Luís de Sousa luis (dot) de (dot) sousa (at) protonmail (dot) ch
# Date: 20-11-2018
###############################################################################

# Follow on this example for binning:
# https://github.com/r-barnes/dggridR/issues/6

install.packages('rgdal')
install.packages('colorspace')
install.packages('ggplot2')
install.packages('dggridR')

library(dggridR)
library(dplyr)

# Construct a grid of resolution 7
dggs <- dgconstruct(res=7)

# Sequence of cell ids
allSeqs <- seq(1, dgmaxcell(dggs))

# Obtain cell centres
cellCentres <- dgSEQNUM_to_GEO(dggs, allSeqs)

# Create data.frame with cellCentres and cell ids
df.cellCentres <- data.frame(allSeqs, cellCentres[[1]], cellCentres[[2]])
colnames(df.cellCentres) <- c("id", "lon", "lat")
saveRDS(df.cellCentres, file = "data/df.cellCentres.rds")

# Save shapefile of cells for mapping
dgcellstogrid(dggs, allSeqs, frame=TRUE, wrapcells=TRUE, savegrid="data/cells/cells.shp")

# Next: writing points to a GeoJSON:
# https://gis.stackexchange.com/questions/147219/how-can-i-export-a-geojson-file-from-r
# spatialpointsdataframe
# https://stackoverflow.com/questions/32583606/create-spatialpointsdataframe
