# MAkes nice graphs from CSV files
#
# Author: Luís de Sousa luis (dot) de (dot) sousa (at) protonmail (dot) ch
# Date: 10-12-2018
###############################################################################

# Snippets from:
# https://matplotlib.org/gallery/pyplots/boxplot_demo_pyplot.html#sphx-glr-gallery-pyplots-boxplot-demo-pyplot-py

import pandas as pd
import matplotlib.pyplot as plt

pathInput = "data/land/"
suffix = "Land"

def main():

    hammer     = pd.read_csv(pathInput + 'Hammer.csv')
    eckertIV   = pd.read_csv(pathInput + 'EckertIV.csv')
    mollweide  = pd.read_csv(pathInput + 'Mollweide.csv')
    sinusoidal = pd.read_csv(pathInput + 'Sinusoidal.csv')
    homolosine = pd.read_csv(pathInput + 'Homolosine.csv')

    angles = [
        sinusoidal.Angle.values.astype(float),
        mollweide.Angle.values.astype(float),
        hammer.Angle.values.astype(float),
        eckertIV.Angle.values.astype(float),
        homolosine.Angle.values.astype(float)
        ]

    distances = [
        sinusoidal.Distance.values.astype(float),
        mollweide.Distance.values.astype(float),
        hammer.Distance.values.astype(float),
        eckertIV.Distance.values.astype(float),
        homolosine.Distance.values.astype(float)
        ]

    labels = ["Sinusoidal", "Mollweide", "Hammer", "Eckert IV", "Homolosine"]

    plt.ioff()
    fig1, ax1 = plt.subplots()
    ax1.set_title('Angular Distortion ' + suffix)
    ax1.boxplot(angles, labels=labels, showfliers=False)
    fig1.savefig('graphs/boxplotAngles' + suffix + '.pdf', bbox_inches='tight')   # save the figure to file
    plt.close(fig1)

    fig1, ax1 = plt.subplots()
    axes = plt.gca()
    axes.set_ylim([0,700])
    ax1.set_title('Absolute Distance Distortion ' + suffix)
    ax1.boxplot(distances, labels=labels, showfliers=False)
    fig1.savefig('graphs/boxplotDistances' + suffix + '.pdf', bbox_inches='tight')   # save the figure to file
    plt.close(fig1)


if __name__ == '__main__': main()
