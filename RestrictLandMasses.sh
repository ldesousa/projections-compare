#!/bin/sh
# Restricts the centres of the Tissot Indicatrcies to those falling inland.
#
# Author: Luís de Sousa luis (dot) de (dot) sousa (at) protonmail (dot) ch
# Date: 26-12-2018
###############################################################################

pathCountries="/home/lads/git/ProjectionsCompare/data/NaturalEarth/ne_10m_admin_0_countries.shp"
pathCentres="/home/lads/git/ProjectionsCompare/data/centres.gpkg"
pathOutput="/home/lads/git/ProjectionsCompare/data/centresLand.gpkg"

v.in.ogr input=$pathCountries output="countries" --overwrite
v.in.ogr input=$pathCentres output="centres" --overwrite
v.select ainput="centres" binput="countries" output="centresLand" operator="overlap" --overwrite
v.out.ogr input="centresLand" output=$pathOutput format="GPKG" --overwrite
g.remove type=vector name="centres,countries,centresLand" -f
