Comparison of FOSS4G supported equal-area projections
===============================================================================


Copyright
-------------------------------------------------------------------------------

Copyright (c) 2018-2019 Luís Moreira de Sousa. All rights reserved.
Any use of this software constitutes full acceptance of all terms of the
document licence.

Description
-------------------------------------------------------------------------------

This set of programmes compares the performance of five equal-area projections
supported by Free and Open Source Software for Geo-spatial (FOSS4G):

- Sinusoidal
- Mollweide
- Hammer
- Eckert IV
- Homolosine

The comparison is made using discrete distortion indicatrices positioned on the 
ellipsoid according to a Icosahedral Snyder Equal-area Grid (ISEAG).

Licence
-------------------------------------------------------------------------------

This suite of programmes is released under the [EUPL 1.2 licence](https://joinup.ec.europa.eu/community/eupl/og_page/introduction-eupl-licence).
For full details please consult the LICENCE file.
