# Creates a discrete counter-domain for projections with a single interruption
# along the 180º meridian.
#
# Author: Luís de Sousa luis (dot) de (dot) sousa (at) protonmail (dot) ch
# Date: 02-02-2019
###############################################################################

outputFile = "data/CounterDomain180.asc"

def main():

    print("Creating counter domain ...")
    file = open(outputFile, 'w+')

    file.write('VERTI:\nA  360\n')

    for lat in range(-90, 91):
        file.write(' -180 '+str(lat)+'\n')

    for lat in range(90, -91, -1):
        file.write(' 180 '+str(lat)+'\n')

    file.close()
    print("All done!")


if __name__ == '__main__': main()
