# Saves the centres of the Tissot Indicatrcies to a GeoPackage.
#
# Author: Luís de Sousa luis (dot) de (dot) sousa (at) protonmail (dot) ch
# Date: 27-12-2018
###############################################################################

from osgeo import ogr
from osgeo import osr
import rpy2.robjects as robjects
from rpy2.robjects import pandas2ri

centresFile = "data/centres.gpkg"
centres = None
dataSource = None


def createOutputFile():

    global centres
    global dataSource
    driver = ogr.GetDriverByName("GPKG")
    dataSource = driver.CreateDataSource(centresFile)
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(4326)
    centres = dataSource.CreateLayer("Centres", srs, ogr.wkbPoint)


def processCentres(df):

    global centres
    for i in range(0, len(df[0])):
        geom = ogr.Geometry(ogr.wkbPoint)
        geom.AddPoint(df[0][i], df[1][i])
        feature = ogr.Feature(centres.GetLayerDefn())
        feature.SetGeometry(geom)
        centres.CreateFeature(feature)


def main():

    pandas2ri.activate()
    readRDS = robjects.r['readRDS']
    df = readRDS('data/cellCentres.rds')
    df = pandas2ri.ri2py(df)
    # df[0]: longitudes  df[1]: latitudes
    numCells = len(df[0])
    print("Length of the data.frame: ", numCells)

    createOutputFile()
    processCentres(df)
    dataSource = None

    print("All done!")


if __name__ == '__main__': main()
