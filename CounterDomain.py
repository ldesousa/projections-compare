# Creates a discrete counter-domain for projections with a single interruption
# along the 180º meridian.
#
# Author: Luís de Sousa luis (dot) de (dot) sousa (at) protonmail (dot) ch
# Date: 27-01-2019
###############################################################################

from osgeo import ogr
from osgeo import osr

outputFile = "data/CounterDomain180.gpkg"
dataSource = None
counterDomain = None


def createOutputFile():

    global dataSource
    global counterDomain
    driver = ogr.GetDriverByName("GPKG")
    dataSource = driver.CreateDataSource(outputFile)
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(4326)
    counterDomain = dataSource.CreateLayer("CounterDomain", srs, ogr.wkbPolygon)


def createPolygon():

    global counterDomain
    ring = ogr.Geometry(ogr.wkbLinearRing)

    for lat in range(-90, 91):
        ring.AddPoint(-180, lat)

    for lat in range(90, -91, -1):
        ring.AddPoint(180, lat)

    ring.AddPoint(180, -90)

    geom = ogr.Geometry(ogr.wkbPolygon)
    geom.AddGeometry(ring)
    feature = ogr.Feature(counterDomain.GetLayerDefn())
    feature.SetGeometry(geom)
    counterDomain.CreateFeature(feature)


def main():

    print("Creating counter domain ...")
    createOutputFile()
    createPolygon()
    dataSource = None
    print("All done!")


if __name__ == '__main__': main()
